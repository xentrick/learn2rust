fn main() {
	let _s1 = gives_ownership();

	let s2 = String::from("hello");

	let _s3 = takes_back(s2);
}

fn gives_ownership() -> String {
	let some_string = String::from("hello");

	return some_string;
}

fn takes_back(a_string: String) -> String {
	return a_string;
}

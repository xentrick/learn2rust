#[derive(Debug)]
struct Structure(i32);

// Make it printable in another structure
#[derive(Debug)]
struct Deep(Structure);

fn main() {
	println!("{:?} months in a year.", 12);
	println!("{1:?} {0:?} is the {actor:?} name.",
		"Slater",
		"Christian",
		actor="actor's");
	
	// `Structure` is printable!
	println!("Now {:?} will print!", Structure(3));

	// No control over how results look with `derive`. What if I want this to just show a `7`?
	println!("Now {:?} will print!", Deep(Structure(7)));
}

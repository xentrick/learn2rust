fn main() {
	println!("{} days", 31);

	// arg reuse
	println!("{}, this is {1}. {1}, this is {0}", "Nick", "Spenser");

	// named arguments
	println!("{subject} {verb} {object}",
		object="the lazy dog",
		subject="the quick brown fox",
		verb="jumps over");
	
	// Special formatting after a :
	println!("{} of {:b} people know binary, the other half doesn't", 1, 2);
	
	// right align text
	println!("{number:>width$}", number=1, width=6);
	
	// pad with zeros
	println!("{number:>0width$}", number=1, width=6);
}
